import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HotelsComponent } from './hotels/hotels.component';
import { HomeComponent } from './home/home.component';
import { VoyagesComponent } from './voyages/voyages.component';
import { ContactComponent } from './contact/contact.component';
import { SingleHotelComponent } from './single-hotel/single-hotel.component';
import { ConfirmReservationComponent } from './confirm-reservation/confirm-reservation.component';
import { SaveToPdfComponent } from './save-to-pdf/save-to-pdf.component';


const routes: Routes = [
  {path:'hotels', component:HotelsComponent},
  {path:'hotels/:id', component:SingleHotelComponent},
  {path:'home', component:HomeComponent},
  {path:'voyages', component:VoyagesComponent},
  {path:'contact', component:ContactComponent},
  {path:'confirmReservation', component:ConfirmReservationComponent},
  {path:'saveToPdf',component:SaveToPdfComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
