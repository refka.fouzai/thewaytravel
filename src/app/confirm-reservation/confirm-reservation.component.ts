import { Component, OnInit } from '@angular/core';
import { DetailReservation } from '../models/detail-reservation';
import { CommonServiceService } from '../services/common-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirm-reservation',
  templateUrl: './confirm-reservation.component.html',
  styleUrls: ['./confirm-reservation.component.css']
})
export class ConfirmReservationComponent implements OnInit {
  Age:number[] = [1,2,3,4,5,6,7,8,9,10,11,12];
  reservation: DetailReservation;
  constructor(private commonService:CommonServiceService,
              private router:Router) { }

  ngOnInit() {
    this.commonService.cast2.subscribe(reser=>{
      this.reservation = reser;
      ///console.log(this.reservation);
      });
     // console.log(this.reservation);
  }
  confirmReser(){
    console.log(this.reservation);
    this.commonService.viewReservationClient(this.reservation);
    this.router.navigate(['saveToPdf']);
  }

}
