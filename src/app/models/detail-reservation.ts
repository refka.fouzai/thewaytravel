import { Client } from './client';
import { VenteHotel } from './vente-hotel';
import { Devis } from './devis';

export class DetailReservation {
    
    client = new Client;
    vente_hotel = new VenteHotel;
    Chambre: Devis[] = [];
}
