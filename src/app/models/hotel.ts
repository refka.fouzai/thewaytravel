export class Hotel {
    id:number;
    nom:string;
    ville:string;
    categorie:number;
    lpdvente:number;
    dpvente:number;
    pcvente:number;
    allinsoftvente:number;
    allinvente:number;
    ultraallinvente:number;
    dispo:Date[];
}
