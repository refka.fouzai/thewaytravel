import { Enfant } from './enfant';
import { Adulte } from './adulte';

export class Devis {
    number_adulte = -1;
    number_enfant = -1;
    arrangement = "";
    type_chambre:number;
    prix = 0;
    adultes: Adulte[] = [];
    enfants : Enfant[] =[];
}
