import { Component, OnInit } from '@angular/core';
import { ServiceVilleService } from '../services/service-ville.service';
import { Ville } from '../models/ville';
import { VoyageService } from '../services/voyage.service';
import { Voyage } from '../models/voyage';

@Component({
  selector: 'app-voyages',
  templateUrl: './voyages.component.html',
  styleUrls: ['./voyages.component.css']
})
export class VoyagesComponent implements OnInit {
  voyage : string = "voyage";
  voyages: Voyage[];
  villes: Ville[];
  constructor(private serviceVille:ServiceVilleService, private serviceVoyage:VoyageService) { }

  ngOnInit() {
      this.getVoyages();
      this.getVilles();
    
  }
  getVoyages():void{
    this.serviceVoyage.getVoyages()
    .subscribe(voy=>
      {
        this.voyages = voy,
      console.log(voy)
    },
    err => console.log(err)
    );
  }
  getVilles():void{
    this.serviceVille.getVilles()
    .subscribe(villes=>
      {this.villes = villes,
      console.log(villes)
    },
    err => console.log(err));
  }

}
