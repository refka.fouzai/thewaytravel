import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Ville } from '../models/ville';
import { DateRangePickerComponent, DateRange } from '@syncfusion/ej2-angular-calendars';
import { DispoHotel } from '../models/dispo-hotel';
import { ServiceHotelService } from '../services/service-hotel.service';
import { Hotel } from '../models/hotel';
import { CommonServiceService } from '../services/common-service.service';
import { Disponibility } from '../models/disponibility';


@Component({
  selector: 'app-search-bar-section',
  templateUrl: './search-bar-section.component.html',
  styleUrls: ['./search-bar-section.component.css']
})
export class SearchBarSectionComponent implements OnInit {
  @Input() villes :Ville[];
  @Output() messageEvent = new EventEmitter<Hotel[]>();
  hotels : Hotel[];
  dispoHotel = new DispoHotel;
  checkIn_checkOut :  Date[];
  ville : string;
  public today: Date = new Date();
  public currentYear: number = this.today.getFullYear();
  public currentMonth: number = this.today.getMonth();
  public currentDay: number = this.today.getDate();
  public start: Date = new Date(this.currentYear, this.currentMonth, this.currentDay + 1 );
  public end: Date =  new Date(this.currentYear, this.currentMonth, this.currentDay +2);
  public dateformat:string ="yyyy/MM/dd";
  
  constructor(private serviceHotel:ServiceHotelService, private commonService:CommonServiceService) { 
  }

  ngOnInit() {
    this.commonService.cast.subscribe(res=>{
      this.hotels = res;
      console.log('nouvelle liste hotels'+this.hotels);
      });
    this.commonService.cast1.subscribe(res=>{
        this.checkIn_checkOut = res;
      console.log('date checkin et checkout'+this.checkIn_checkOut);
        });

  }
  deposit() {
    console.log('villle'+this.dispoHotel.destination_city);
    this.dispoHotel.check_in=this.start;
    this.dispoHotel.check_out=this.end;
    //save the checkin checkout date on a shared service to be used on the date range picker of single hotel
    this.checkIn_checkOut = [];
    this.checkIn_checkOut.push(this.start);
    this.checkIn_checkOut.push(this.end);
    this.commonService.viewDateINOUT(this.checkIn_checkOut);
    //console.log(this.dispoHotel);
   //Get new list of hotels with disponibility
    this.serviceHotel.getDisponibiliteHotel(this.dispoHotel)
    .subscribe(listHotel=>
      {
        this.hotels = listHotel,
        this.getDisponibility(this.hotels,this.dispoHotel.check_in,this.dispoHotel.check_out),
        this.filterByCountry(this.dispoHotel.destination_city),
        this.commonService.viewHotels(this.hotels)
       // console.log(listHotel)
      },
      err => console.log(err)
    );
    //this.commonService.viewHotels(this.hotels);
}
 getDisponibility(hotels:Hotel[], checkIn:Date, checkOut:Date){
  var Obj = new Disponibility;
  var date: Date[];
  Obj.in = checkIn;
  Obj.out = checkOut;
  
  for (let i=0; i< hotels.length; i++) { 
    // ... 
    Obj.id = hotels[i].id;
    this.serviceHotel.getHotelInfo(Obj)
    .subscribe(dat=>
      {
        hotels[i].dispo = dat,
        console.log(dat)
       
      },
      err => console.log(err)
    );}

 }
 filterByCountry(ville:string){
  let obj : Array<any> = [];
  for(let i=0;i<this.hotels.length;i++){
    if(this.hotels[i].ville === ville){
      obj.push(this.hotels[i]);
    }
  }
  this.hotels = obj;
 }
}
