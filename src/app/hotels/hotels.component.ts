import { Component, OnInit } from '@angular/core';
import { ServiceHotelService } from '../services/service-hotel.service';
import { Hotel } from '../models/hotel';
import { ServiceVilleService } from '../services/service-ville.service';
import { Ville } from '../models/ville';


@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {
  hotel : string = "hotel";
  idville:number;
  data=  {
    "check_in":'2019-08-29',
    "check_out": '2019-09-05', 
    "destination_city": '1'
    };
  //range:string;
  hotels: Hotel[];
  villes: Ville[];
  constructor(private serviceHotel:ServiceHotelService, private serviceVille:ServiceVilleService) { }
  
  ngOnInit() {
    this.getHotels();
    this.getVilles();
  }
  getHotels():void{
    this.serviceHotel.getHotels()
    .subscribe(hotels=>
      {
        this.hotels = hotels,
      console.log(hotels)
    },
    err => console.log(err)
    );
  }
  getVilles():void{
    this.serviceVille.getVilles()
    .subscribe(villes=>
      {this.villes = villes,
      console.log(villes)
    },
    err => console.log(err));
  }
  
  affichedate(date:any){
    console.log('coucou c moi'+date);
  }
  
  }
