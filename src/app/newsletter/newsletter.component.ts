import { Component, OnInit } from '@angular/core';
import { HomeService } from '../services/home.service';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.css']
})
export class NewsletterComponent implements OnInit {
  email:string;
  constructor(private serviceHome:HomeService) { }

  ngOnInit() {
  }
addNewsLetter(){
  this.serviceHome.addNewsletter(this.email)
    .subscribe(
      res=>console.log(res),
      err=>console.log(err)
    );
}
}
