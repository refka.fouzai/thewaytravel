import { Component, OnInit, Input } from '@angular/core';
import { Devis } from '../models/devis';
import { ResultArrangement } from '../models/result-arrangement';
import { Enfant } from '../models/enfant';
import { Adulte } from '../models/adulte';

@Component({
  selector: 'app-chambre',
  templateUrl: './chambre.component.html',
  styleUrls: ['./chambre.component.css']
})
export class ChambreComponent implements OnInit {
  @Input() ChambresAReserver : Devis[];
  @Input() list1Arrangement:string[];// Liste des arrangements récupérés depuis singleHotelCompoent
  @Input() nbrChambre : number; // numero de chambre ajoutée = numéro d'instance de component inséré
  @Input() arrangement:ResultArrangement;
  constructor() { }

  ngOnInit() {
  }
  onOptionsSelected(event,numChambre) {
    console.log(event.target.name);
    console.log(event.target.value);
    if(event.target.name == "adulte"){
      this.ChambresAReserver[numChambre].number_adulte = event.target.value; 
      this.ChambresAReserver[numChambre].adultes = [];
      for(let i=0;i<event.target.value;i++){
        this.ChambresAReserver[numChambre].adultes.push(new Adulte);
      }
      //this.ChambresAReserver[numChambre].adultes = [event.target.value];
    }
    if(event.target.name == "enfant"){
      this.ChambresAReserver[numChambre].number_enfant = event.target.value;
      this.ChambresAReserver[numChambre].enfants = [];
      for(let i=0;i<event.target.value;i++){
        this.ChambresAReserver[numChambre].enfants.push(new Enfant);
      }
      //this.ChambresAReserver[numChambre].enfants = [event.target.value];
    }
    if(event.target.name == "arrangement"){
      console.log("coucou arrangement");
      this.ChambresAReserver[numChambre].arrangement = event.target.value;
    }
    //this.ChambresAReserver[numChambre].prix = 0 ;
    console.log(this.ChambresAReserver[numChambre].number_adulte+'-,'+this.ChambresAReserver[numChambre].number_enfant+'-,'+this.ChambresAReserver[numChambre].arrangement);
    
    this.calcPrix(this.ChambresAReserver[numChambre]);
   

}
calcPrix(d:Devis){
  if(d.number_adulte == 0)
  {
      if(d.number_enfant == 1)
      {
        //Chambre type 2
        d.type_chambre = 2;
        if(d.arrangement == "LPD")
        {
          d.prix = this.arrangement.chambre[1].LPD ;
         // d.prix = 50.4;
         } 
         if(d.arrangement == "DP")
              {
                    d.prix = this.arrangement.chambre[1].DP;
              } 
         if(d.arrangement == "PC"){
                        d.prix = this.arrangement.chambre[1].PC;
                  } 
         if(d.arrangement == "All_In"){
              d.prix = this.arrangement.chambre[1].All_In;
           }
           if(d.arrangement == "All_In_Soft"){
            d.prix = this.arrangement.chambre[1].All_In_Soft;
          }
          if(d.arrangement == "Ultra_All_In"){
            d.prix = this.arrangement.chambre[1].Ultra_All_In;
          }
          if(d.arrangement == "Lit_BB"){
            d.prix = this.arrangement.chambre[1].Lit_BB;
          }
      } 
      if(d.number_enfant == 2)
      {
              //Chambre type 5
        d.type_chambre = 5;
        if(d.arrangement == "LPD")
        {
             d.prix = this.arrangement.chambre[4].LPD;
        }  
        if(d.arrangement == "DP")
        {
            d.prix = this.arrangement.chambre[4].DP;
        }
        if(d.arrangement == "PC")
        {
            d.prix =this.arrangement.chambre[4].PC;
        }
        if(d.arrangement == "All_In")
        {
            d.prix =this.arrangement.chambre[4].All_In;
        }
        if(d.arrangement == "All_In_Soft"){
          d.prix = this.arrangement.chambre[4].All_In_Soft;
        }
        if(d.arrangement == "Ultra_All_In"){
          d.prix = this.arrangement.chambre[4].Ultra_All_In;
        }
        if(d.arrangement == "Lit_BB"){
          d.prix = this.arrangement.chambre[4].Lit_BB;
        }
      }
      if(d.number_enfant == 3)
      {
          //Chambre type 9
        d.type_chambre = 9;
        if(d.arrangement == "LPD")
        {
            d.prix = this.arrangement.chambre[8].LPD;
        }  
        if(d.arrangement == "DP")
        {
           d.prix = this.arrangement.chambre[8].DP;
        }
        if(d.arrangement == "All_In")
        {
           d.prix = this.arrangement.chambre[8].All_In;
        }
        if(d.arrangement == "PC")
        {
           d.prix =this.arrangement.chambre[8].PC;
        }
        if(d.arrangement == "All_In_Soft"){
          d.prix = this.arrangement.chambre[8].All_In_Soft;
        }
        if(d.arrangement == "Ultra_All_In"){
          d.prix = this.arrangement.chambre[8].Ultra_All_In;
        }
        if(d.arrangement == "Lit_BB"){
          d.prix = this.arrangement.chambre[8].Lit_BB;
        }
      }
      if(d.number_enfant == 4)
      {
         //Chambre type 14
         d.type_chambre = 14;
        if(d.arrangement == "LPD")
        {
           d.prix = this.arrangement.chambre[13].LPD;
        }  
        if(d.arrangement == "DP")
        {
           d.prix = this.arrangement.chambre[13].DP;
        }
        if(d.arrangement == "All_In")
        {
            d.prix = this.arrangement.chambre[13].All_In;
        }
        if(d.arrangement == "PC"){
            d.prix =this.arrangement.chambre[13].PC;
        }
        if(d.arrangement == "All_In_Soft"){
          d.prix = this.arrangement.chambre[13].All_In_Soft;
        }
        if(d.arrangement == "Ultra_All_In"){
          d.prix = this.arrangement.chambre[13].Ultra_All_In;
        }
        if(d.arrangement == "Lit_BB"){
          d.prix = this.arrangement.chambre[13].Lit_BB;
        }
      }
          
    
  }
  if(d.number_adulte == 1)
  {
    if(d.number_enfant == 0)
    {
      //Chambre de type 1
      d.type_chambre = 1;

      if(d.arrangement == "LPD")
      {
           d.prix = this.arrangement.chambre[0].LPD;
       } 
      if(d.arrangement == "DP")
      {
        d.prix = this.arrangement.chambre[0].DP;
      } 
      if(d.arrangement == "All_In_Soft"){
        d.prix = d.prix = this.arrangement.chambre[0].All_In_Soft;
      } 
      if(d.arrangement == "PC"){
        d.prix = this.arrangement.chambre[0].PC;
      }
      if(d.arrangement == "All_In_Soft"){
        d.prix = this.arrangement.chambre[0].All_In_Soft;
      }
      if(d.arrangement == "Ultra_All_In"){
        d.prix = this.arrangement.chambre[0].Ultra_All_In;
      }
      if(d.arrangement == "Lit_BB"){
        d.prix = this.arrangement.chambre[0].Lit_BB;
      }
      
                  
    }
    if(d.number_enfant == 1)
    {
        //chambre type 4
      d.type_chambre = 4;
      if(d.arrangement == "LPD")
      {
         d.prix = this.arrangement.chambre[3].LPD;
      }  
      if(d.arrangement == "DP")
      {
         d.prix = this.arrangement.chambre[3].DP;
      }
      if(d.arrangement == "PC")
      {
         d.prix = this.arrangement.chambre[3].PC;
      }
      if(d.arrangement == "All_In")
      {
         d.prix = this.arrangement.chambre[3].All_In;
      }
      if(d.arrangement == "All_In_Soft"){
        d.prix = this.arrangement.chambre[3].All_In_Soft;
      }
      if(d.arrangement == "Ultra_All_In"){
        d.prix = this.arrangement.chambre[3].Ultra_All_In;
      }
      if(d.arrangement == "Lit_BB"){
        d.prix = this.arrangement.chambre[3].Lit_BB;
      }
      
    }
    if(d.number_enfant == 2)
    {
        //chambre type 8
       d.type_chambre = 8;
      if(d.arrangement == "LPD")
      {
         d.prix = this.arrangement.chambre[7].LPD;
      } 
      if(d.arrangement == "DP")
      {
         d.prix = this.arrangement.chambre[7].DP;
      }
      if(d.arrangement == "PC")
      {
         d.prix = this.arrangement.chambre[7].PC;
      }
      if(d.arrangement == "All_In")
      {
         d.prix = this.arrangement.chambre[7].All_In;
      }
      if(d.arrangement == "All_In_Soft"){
        d.prix = this.arrangement.chambre[7].All_In_Soft;
      }
      if(d.arrangement == "Ultra_All_In"){
        d.prix = this.arrangement.chambre[7].Ultra_All_In;
      }
      if(d.arrangement == "Lit_BB"){
        d.prix = this.arrangement.chambre[7].Lit_BB;
      }
     
    }
    if(d.number_enfant == 3)
    {
        //chambre type 13 
       d.type_chambre = 13;
      if(d.arrangement == "LPD")
      {
         d.prix = this.arrangement.chambre[12].LPD;
      } 
      if(d.arrangement == "DP")
      {
         d.prix = this.arrangement.chambre[12].DP;
      }
      if(d.arrangement == "PC")
      {
         d.prix = this.arrangement.chambre[12].PC;
      }
      if(d.arrangement == "All_In")
      {
         d.prix = this.arrangement.chambre[12].All_In;
      }
      if(d.arrangement == "All_In_Soft"){
        d.prix = this.arrangement.chambre[12].All_In_Soft;
      }
      if(d.arrangement == "Ultra_All_In"){
        d.prix = this.arrangement.chambre[12].Ultra_All_In;
      }
      if(d.arrangement == "Lit_BB"){
        d.prix = this.arrangement.chambre[12].Lit_BB;
      }
      
    }
  }
  if(d.number_adulte == 2)
  {
      if(d.number_enfant == 0)
      {
        //Chambre de type 3
        d.type_chambre = 3;
        if(d.arrangement == "LPD")
        {
           d.prix = this.arrangement.chambre[2].LPD;
        } 
        if(d.arrangement == "DP")
        {
           d.prix = this.arrangement.chambre[2].DP;
        }
        if(d.arrangement == "PC")
        {
           d.prix = this.arrangement.chambre[2].PC;
        }
        if(d.arrangement == "All_In")
        {
           d.prix = this.arrangement.chambre[2].All_In;
        }
        if(d.arrangement == "All_In_Soft"){
          d.prix = this.arrangement.chambre[2].All_In_Soft;
        }
        if(d.arrangement == "Ultra_All_In"){
          d.prix = this.arrangement.chambre[2].Ultra_All_In;
        }
        if(d.arrangement == "Lit_BB"){
          d.prix = this.arrangement.chambre[2].Lit_BB;
        }
        
      }
      if(d.number_enfant == 1)
      {
          //Chambre de type 7
          d.type_chambre = 7;
        if(d.arrangement == "LPD")
        {
          d.prix = this.arrangement.chambre[6].LPD;
        }  
        if(d.arrangement == "DP")
        {
          d.prix = this.arrangement.chambre[6].DP;
        }
        if(d.arrangement == "PC")
        {
           d.prix = this.arrangement.chambre[6].PC;
        }
        if(d.arrangement == "All_In")
        {
           d.prix = this.arrangement.chambre[6].All_In;
        }
        if(d.arrangement == "All_In_Soft"){
          d.prix = this.arrangement.chambre[6].All_In_Soft;
        }
        if(d.arrangement == "Ultra_All_In"){
          d.prix = this.arrangement.chambre[6].Ultra_All_In;
        }
        if(d.arrangement == "Lit_BB"){
          d.prix = this.arrangement.chambre[6].Lit_BB;
        }
        

      } 
      if(d.number_enfant == 2)
      {
          //Chambre de type 12
        d.type_chambre = 12;
        if(d.arrangement == "LPD")
        {
          d.prix = this.arrangement.chambre[11].LPD;
        }  
        if(d.arrangement == "DP")
        {
          d.prix = this.arrangement.chambre[11].DP;
        }
        if(d.arrangement == "PC")
        {
          d.prix = this.arrangement.chambre[11].PC;
        }
        if(d.arrangement == "All_In")
        {
          d.prix = this.arrangement.chambre[11].All_In;
        }
        if(d.arrangement == "All_In_Soft"){
          d.prix = this.arrangement.chambre[11].All_In_Soft;
        }
        if(d.arrangement == "Ultra_All_In"){
          d.prix = this.arrangement.chambre[11].Ultra_All_In;
        }
        if(d.arrangement == "Lit_BB"){
          d.prix = this.arrangement.chambre[11].Lit_BB;
        }
        
      }

    }
    if(d.number_adulte == 3)
    {
      if(d.number_enfant == 0)
      {
        //Chambre de type 6
        d.type_chambre = 6;
      if(d.arrangement == "LPD")
      {
         d.prix = this.arrangement.chambre[5].LPD;
      } 
      if(d.arrangement == "DP")
      {
         d.prix = this.arrangement.chambre[5].DP;
      }
      if(d.arrangement == "All_In")
      {
         d.prix = this.arrangement.chambre[5].All_In;
      } 
      if(d.arrangement == "PC")
      {
         d.prix = this.arrangement.chambre[5].All_In;
      }
      if(d.arrangement == "All_In_Soft"){
        d.prix = this.arrangement.chambre[5].All_In_Soft;
      }
      if(d.arrangement == "Ultra_All_In"){
        d.prix = this.arrangement.chambre[5].Ultra_All_In;
      }
      if(d.arrangement == "Lit_BB"){
        d.prix = this.arrangement.chambre[5].Lit_BB;
      }
      
    }
    if(d.number_enfant == 1)
    {
        //chambre type 11
        d.type_chambre = 11;
      if(d.arrangement == "LPD")
      {
         d.prix = this.arrangement.chambre[10].LPD;
      } 
      if(d.arrangement == "DP")
      {
         d.prix = this.arrangement.chambre[10].DP;
      }
      if(d.arrangement == "All_In")
      {
         d.prix = this.arrangement.chambre[10].All_In;
      }
      if(d.arrangement == "PC")
      {
         d.prix = this.arrangement.chambre[10].PC;
      }
      if(d.arrangement == "All_In_Soft"){
        d.prix = this.arrangement.chambre[10].All_In_Soft;
      }
      if(d.arrangement == "Ultra_All_In"){
        d.prix = this.arrangement.chambre[10].Ultra_All_In;
      }
      if(d.arrangement == "Lit_BB"){
        d.prix = this.arrangement.chambre[10].Lit_BB;
      }
      
      }

    }
    if(d.number_adulte == 4)
    {
      if(d.number_enfant == 0)
      {
      //Chambre de type 10
      d.type_chambre = 10;
      if(d.arrangement == "LPD")
      {
         d.prix = this.arrangement.chambre[9].LPD;
      }  
      if(d.arrangement == "DP")
      {
         d.prix = this.arrangement.chambre[9].DP;
      }
      if(d.arrangement == "All_In")
      {
         d.prix = this.arrangement.chambre[9].All_In;
      } 
      if(d.arrangement == "PC")
      {
        d.prix = this.arrangement.chambre[9].PC;
      }
      if(d.arrangement == "All_In_Soft"){
        d.prix = this.arrangement.chambre[9].All_In_Soft;
      }
      if(d.arrangement == "Ultra_All_In"){
        d.prix = this.arrangement.chambre[9].Ultra_All_In;
      }
      if(d.arrangement == "Lit_BB"){
        d.prix = this.arrangement.chambre[9].Lit_BB;
      }
      }
      

    }
    
    if((d.number_adulte>=0)&&(d.number_enfant>=0)&&(d.arrangement!='')){
      console.log('votre chambre est de type :'+d.type_chambre+'('+d.number_adulte+','+d.number_enfant+','+d.arrangement+') avec total = '+d.prix+'DT')
    }
}

}
