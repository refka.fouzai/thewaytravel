import { Component, OnInit } from '@angular/core';
import { HomeService } from '../services/home.service';
import { Tour } from '../models/tour';
import { DomSanitizer } from '@angular/platform-browser';
import { ServiceVilleService } from '../services/service-ville.service';
import { Ville } from '../models/ville';

@Component({
  selector: 'app-body-home',
  templateUrl: './body-home.component.html',
  styleUrls: ['./body-home.component.css']
})
export class BodyHomeComponent implements OnInit {
  tours:Tour[];
  villes: Ville[];
  constructor(private serviceHome:HomeService, private serviceVille:ServiceVilleService,private sanitizer: DomSanitizer ) { }

  ngOnInit() {
    this.serviceVille.getVilles()
    .subscribe(vil=>
      {
        this.villes = vil,
        console.log(vil);
      });
    this.serviceHome.getImagesCaroussel()
      .subscribe(img=>
        {
          this.tours = img,
          console.log(img)
        },
        err => console.log('coucou erreur'+err)
        );
  }
  
  
    public getSanitizedUrl(img:string) {
      console.log(this.sanitizer.bypassSecurityTrustUrl(img));
        return this.sanitizer.bypassSecurityTrustUrl(img);
    
  
    }
}
