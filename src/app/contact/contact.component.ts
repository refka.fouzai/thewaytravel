import { Component, OnInit } from '@angular/core';
import { Contact } from '../models/contact';
import { ServiceContactService } from '../services/service-contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contact = new Contact;
  constructor(private serviceContact:ServiceContactService) { }

  ngOnInit() {
  }
  addContact(){
    this.serviceContact.addContact(this.contact)
    .subscribe(
      res=>console.log(res),
      err=>console.log(err)
    );
    console.log(this.contact);
    
  }
  

}
