import { Component, OnInit } from '@angular/core';
import { CommonServiceService } from '../services/common-service.service';
import { DetailReservation } from '../models/detail-reservation';
import { ServiceHotelService } from '../services/service-hotel.service';
import {DatePipe} from '@angular/common';
import moment from 'moment';

@Component({
  selector: 'app-save-to-pdf',
  templateUrl: './save-to-pdf.component.html',
  styleUrls: ['./save-to-pdf.component.css']
})
export class SaveToPdfComponent implements OnInit {
  reservation: DetailReservation;
  hotelName:string;
  nuitee: any;
  tableofRoomTypes : string[] = [];
  gotData : Promise<boolean>;
  constructor(private datePipe: DatePipe,private commonService: CommonServiceService,private serviceHotel:ServiceHotelService) { }

  ngOnInit() {
   this.commonService.cast2.subscribe(reser=>
     {
      this.reservation = reser;
      });
    // get hotel name from API
    this.serviceHotel.getHotelById(this.reservation.vente_hotel.hotel)
    .subscribe(hotel=>
      {
        this.hotelName = hotel[0].nom;
        console.log(hotel);
        this.gotData = Promise.resolve(true);
        console.log('hotelName'+this.hotelName);
      },
        err => console.log(err)
      );
      //calcul nombre de nuitée
      this.calcNuit();
      this.getRoomType();
     
      
  }
  getRoomType(){
    
    for(let i = 0;i<this.reservation.Chambre.length;i++)
    {
      let type = this.reservation.Chambre[i].type_chambre;
        if((type == 1)||(type == 2)){
        this.tableofRoomTypes.push("Single");
      }
      
      if((type == 3)||(type == 4)||(type == 5)){
        this.tableofRoomTypes.push("Double") ;
      }
      
      if((type == 6)||(type == 7)||(type == 8)||(type == 9)){
        this.tableofRoomTypes.push("Triple");
      }
    
      if((type == 10)||(type == 11)||(type == 12)||(type == 13)||(type == 14)){
        this.tableofRoomTypes.push("Quadruple");
      }
  }
  }
calcNuit(){
  
  let d1 = this.reservation.vente_hotel.entree;
  let d2 = this.reservation.vente_hotel.sortie;
  console.log(moment(d2).diff(moment(d1), 'days'));
  this.nuitee = moment(d2).diff(moment(d1), 'days');
}
}
