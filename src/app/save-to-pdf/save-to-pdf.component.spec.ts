import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveToPdfComponent } from './save-to-pdf.component';

describe('SaveToPdfComponent', () => {
  let component: SaveToPdfComponent;
  let fixture: ComponentFixture<SaveToPdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveToPdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveToPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
