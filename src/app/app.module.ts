
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {PDFExportModule} from '@progress/kendo-angular-pdf-export';
import {NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';
import { HttpClientModule } from '@angular/common/http'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { VoyagesComponent } from './voyages/voyages.component';
import { HotelsComponent } from './hotels/hotels.component';
import { ContactComponent } from './contact/contact.component';
import { BodyHomeComponent } from './body-home/body-home.component';
import { NewsletterComponent } from './newsletter/newsletter.component';
import { SingleHotelComponent } from './single-hotel/single-hotel.component';
import { DaterangepickerComponent } from './daterangepicker/daterangepicker.component';
import { SearchBarSectionComponent } from './search-bar-section/search-bar-section.component';
import {ResultSearchComponent} from './result-search/result-search.component';
import { SafePipeModule } from 'safe-pipe';
import { DatePipe } from '@angular/common';
import { CommonServiceService } from './services/common-service.service';
import { ServiceHotelService } from './services/service-hotel.service';
import { ServiceVilleService } from './services/service-ville.service';
import { ChambreComponent } from './chambre/chambre.component';
import { ConfirmReservationComponent } from './confirm-reservation/confirm-reservation.component';
import { SaveToPdfComponent } from './save-to-pdf/save-to-pdf.component';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    VoyagesComponent,
    HotelsComponent,
    ContactComponent,
    BodyHomeComponent,
    NewsletterComponent,
    SingleHotelComponent,
    DaterangepickerComponent,
    SearchBarSectionComponent,
    ResultSearchComponent,
    ChambreComponent,
    ConfirmReservationComponent,
    SaveToPdfComponent,
    
    
   
  ],
  imports: [
    BrowserModule,
    SafePipeModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgxDaterangepickerMd,
    DateRangePickerModule,
    DatePickerModule,
    PDFExportModule
    
  ],
  providers: [DatePipe, CommonServiceService, ServiceHotelService,ServiceVilleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
