import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ServiceHotelService } from '../services/service-hotel.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Hotel } from '../models/hotel';
import { DatePipe } from '@angular/common';
import { Carousel } from '../models/carousel';
import { CommonServiceService } from '../services/common-service.service';
import { Arrangement } from '../models/arrangement';
import { ResultArrangement } from '../models/result-arrangement';
import { Devis } from '../models/devis';
import { Client } from '../models/client';
import { VenteHotel } from '../models/vente-hotel';
import { DetailReservation } from '../models/detail-reservation';
declare var $: any;
@Component({
  selector: 'app-single-hotel',
  templateUrl: './single-hotel.component.html',
  styleUrls: ['./single-hotel.component.css']
})

export class SingleHotelComponent implements OnInit {
  images: Carousel;
  idHotel:number;
  hotel: Hotel;
  //ejs-daterangepicker variables
  public today: Date = new Date();
  public currentYear: number = this.today.getFullYear();
  public currentMonth: number = this.today.getMonth();
  public currentDay: number = this.today.getDate();
  public start: Date = new Date(this.currentYear, this.currentMonth, this.currentDay + 1 );
  public end: Date =  new Date(this.currentYear, this.currentMonth, this.currentDay +2);
  public dateformat:string ="yyyy/MM/dd";
  checkInOUt : Date[] = [];  
  hote = new Hotel;
  arrange = new Arrangement;
  arrangement = new ResultArrangement;
  list1 :string[];
  devis = new Devis;
  ChambresAReserver: Devis[] = [];
  nbrChambre = 0;
  prix : number;
  cl = new Client;
  vente = new VenteHotel;
  reservation /*= new DetailReservation*/;
  //test: any[] = [];
  gotData: Promise<boolean>;
  constructor(private route:ActivatedRoute,private router: Router,private serviceHotel:ServiceHotelService,private commonService:CommonServiceService) {}

   ngOnInit() {
     
      this.getDateIN_Out();
      this.getIdHotel();
      this.getHotelById();
      this.initVenteHotel();
      this.getArrangement();
      this.getImagesHotel();  
      this.ChambresAReserver[0] = this.devis;
      this.commonService.cast2.subscribe(reser=>{
        this.reservation = reser;
      
        }); 
      }
      initVenteHotel(){
        this.vente.hotel = this.idHotel;
        this.vente.entree = this.start;
        this.vente.sortie = this.end;
      }
      getDateIN_Out(){
          this.commonService.cast1.subscribe(dates=>{
          this.checkInOUt = dates;
          console.log(this.checkInOUt);
             
              if(this.checkInOUt != null){
                  this.start = this.checkInOUt[0];
                  this.end = this.checkInOUt[1];
              }
    
          });
      }
      getIdHotel(){
       this.idHotel=(this.route.snapshot.params.id).substr(1);
       console.log( this.idHotel);
      }
      getArrangement(){
       
       this.arrange.id = this.idHotel;
       this.arrange.check_in = this.start;
       this.arrange.check_out = this.end;
       this.serviceHotel.getSingleHotelArrangement(this.arrange)
       .subscribe(res=>
        {   this.arrangement = res;
            this.list1 = this.arrangement.arrangement;
            
            console.log(this.arrangement);
            console.log(this.arrangement.chambre);
            console.log("liste des arrangements"+this.arrangement.arrangement);
        },
        err => console.log(err));

      }
   
    getHotelById(){
        this.serviceHotel.getHotelById(this.idHotel)
        .subscribe(hotel=>
          {
            this.hotel = hotel,
            this.gotData = Promise.resolve(true);
          console.log(hotel)
        },
        err => console.log(err)
        );
      }
    getImagesHotel(){
      this.serviceHotel.getHotelCarousel(this.idHotel)
      .subscribe(img=>
        {
          this.images = img
         // console.log('href du carousel'+this.images[0].href)
        },
        err => console.log('coucou erreur'+err)
        );
      /* for (let i = 0; i < this.images.length; i++) {
          console.log(this.images[i]);
        }*/

    }
    
    
  //@ViewChild('block',{static:false}) block:ElementRef;

addChambre() {
  this.nbrChambre++;
  this.ChambresAReserver[this.nbrChambre] = new Devis; 
  //this.block.nativeElement.insertAdjacentHTML('beforeend', '<div class="row" id="nbrChambre"><div class="col-md-3"><div class="form-group" ><select class="form-control"  required (change)="onOptionsSelected($event,nbrChambre)"><option value="0" selected hidden>Adulte(s)</option><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select><span class="select-arrow"></span><span class="form-label">Adulte(s)</span></div></div><div class="col-md-3"><div class="form-group"><select class="form-control" required (change)="onOptionsSelected($event,nbrChambre)"><option value="0" selected hidden>Enfant(s)</option><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select><span class="select-arrow"></span><span class="form-label">Enfant(s)</span></div></div><div class="col-md-3"><div class="form-group"><select class="form-control" required (change)="onOptionsSelected($event,nbrChambre)"><option value="LPD" selected hidden>Arrangement</option><option *ngFor="let arrang of list1 " [value]="arrang">arrang</option></select><span class="select-arrow"></span><span class="form-label">Arrangement</span></div></div><div class="col-md-3"><div class="form-group"><input class="form-control" type="text" [value]="ChambresAReserver[nbrChambre].prix" placeholder="Prix.."><span class="form-label">Prix</span></div></div></div>');
}
reserver(){
    var i:number =0;
    for (let d of this.ChambresAReserver) { 
      console.log('Chambre'+i+'de type :'+d.type_chambre+'('+d.number_adulte+','+d.number_enfant+','+d.arrangement+') avec total = '+d.prix+'DT')
      i++;
    }
    console.log(this.ChambresAReserver);
    //update checkin and checkout dates:
    this.updateCheckInCheckOut();
    this.saveReservation();
    this.router.navigate(['confirmReservation']);
  }
  updateCheckInCheckOut(){
    
       this.vente.entree = this.start;
       this.vente.sortie = this.end;
     
  }
  saveReservation(){
    this.reservation = new DetailReservation;
    this.reservation.vente_hotel = this.vente;
    this.reservation.client = this.cl;
    this.reservation.Chambre = this.ChambresAReserver;
    console.log(this.reservation);
    this.commonService.viewReservationClient(this.reservation);
  }
    
    /*this.serviceHotel.bookHotel1(this.test)
    .subscribe(obj=>
    {
      console.log(obj)
    },
    err => console.log(err)
    );
   /* this.serviceHotel.bookHotel2(this.vente)
    .subscribe(obj=>
    {
      console.log(obj)
    },
    err => console.log(err)
    );*/
    
    
    
  
      
}
