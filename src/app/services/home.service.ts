import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Tour } from '../models/tour';
const httpOptions= {
  headers: new HttpHeaders(
    {'Content-Type':  'application/json'}
    )
};
// imageHotelHref : String[];
const carouselHomePage = "https://freedomtravel.tn/json/carousel_home_page.php";
const newsletterUrl = "https://www.freedomtravel.tn/ng/addnewLEtterForm.php";
@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http:HttpClient) { 

  }
  
  getImagesCaroussel(): Observable<Tour[]> {
    return this.http.get<Tour[]>(carouselHomePage);
  }
  addNewsletter(email:string):Observable<string>{
    return this.http.post<string>(newsletterUrl, email, httpOptions)
  }
}
