import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Ville } from '../models/ville';

const villeUrl =  "https://www.freedomtravel.tn/ng/villes.php";

@Injectable({
  providedIn: 'root'
})
export class ServiceVilleService {

  constructor(private http:HttpClient) { }
  getVilles():Observable<Ville[]>{
    return this.http.get<Ville[]>(villeUrl);
  }
}
