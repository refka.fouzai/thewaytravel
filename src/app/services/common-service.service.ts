import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Hotel } from '../models/hotel';
import { DetailReservation } from '../models/detail-reservation';

@Injectable({
  providedIn: 'root'
})
export class CommonServiceService {

  constructor() { }
  
  private hotels = new BehaviorSubject<Hotel[]>(null);
  cast = this.hotels.asObservable();
  viewHotels(newhotels){
    this.hotels.next(newhotels);
  }
  private CheckIn_Check_Out = new BehaviorSubject<Date[]>(null);
  cast1 = this.CheckIn_Check_Out.asObservable();
  viewDateINOUT(newDate){
    this.CheckIn_Check_Out.next(newDate);
  }
  private ReservationClient = new BehaviorSubject<DetailReservation>(null);
  cast2 = this.ReservationClient.asObservable();
  viewReservationClient(newReser){
    this.ReservationClient.next(newReser);
  }
  
}
