import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Hotel } from '../models/hotel';
import { Carousel } from '../models/carousel';
import { ResultArrangement } from '../models/result-arrangement';
import { Client } from '../models/client';
import { DetailReservation } from '../models/detail-reservation';

const httpOptions= {
  headers: new HttpHeaders(
    {'Content-Type':  'application/json'}
    )
};
// imageHotelHref : String[];
const hotelsUrljson = "https://www.freedomtravel.tn/json/hotelDetails.php";
const hotelsUrl = "https://www.freedomtravel.tn/ng/hotels.php";
const carrouselUrl = "https://www.freedomtravel.tn/ng/carouselHotel.php";
const dispoHotelUrl = "https://www.freedomtravel.tn/ng/disponibilitehotel.php";
const grilleTarifUrl = "https://www.freedomtravel.tn/ng/grilleTarifaireHotel.php";
const ReservationUrl = "https://www.freedomtravel.tn/ng/ajouter_sejour_hotel.php"
const href={"id": '6'};

@Injectable({
  providedIn: 'root'
})
export class ServiceHotelService {

  constructor(private http:HttpClient) {

   }
   getHotels(): Observable<Hotel[]> {
    return this.http.get<Hotel[]>(hotelsUrl);
  }
  
  getHotelById(id:number):Observable<Hotel> {
    let params = new HttpParams().set("id",id.toString());
    return this.http.get<Hotel>(hotelsUrljson,{headers:httpOptions.headers,params:params});
 }
 getHotelCarousel(id:number):Observable<Carousel> {
   href.id = id.toString();
  return this.http.post<Carousel>(carrouselUrl,href,httpOptions);
 }
 getDisponibiliteHotel(dispoHotel:Object):Observable<Hotel[]> {
  return this.http.post<Hotel[]>(hotelsUrl,dispoHotel,httpOptions);
}
getHotelInfo(data:Object):Observable<Date[]> {
  return this.http.post<Date[]>(dispoHotelUrl,data,httpOptions);
}
getSingleHotelArrangement(singleHotel:Object):Observable<ResultArrangement>{
  return this.http.post<ResultArrangement>(grilleTarifUrl, singleHotel, httpOptions)
}
bookHotel1(obj1){
  return this.http.post<object>(ReservationUrl,obj1,httpOptions);
}
bookHotel2(obj2){
  return this.http.post<object>(ReservationUrl,obj2,httpOptions);
}
}
