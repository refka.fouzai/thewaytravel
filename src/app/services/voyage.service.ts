import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Voyage } from '../models/voyage';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions= {
  headers: new HttpHeaders(
    {'Content-Type':  'application/json'}
    )
};
const  data=  {
  "check_in":'2019-08-29',
  "check_out": '2019-09-05', 
  "destination_city": '2'
  };
const voyageURL="https://www.freedomtravel.tn/json/circuit.php";
@Injectable({
  providedIn: 'root'
})
export class VoyageService {

  constructor(private http:HttpClient) { }
  getVoyages(): Observable<Voyage[]> {
    return this.http.get<Voyage[]>(voyageURL);
  }
  getVoyagewithVilleAndDate():Observable<Voyage[]> {
    return this.http.post<Voyage[]>(voyageURL,data,httpOptions);
  }
}
