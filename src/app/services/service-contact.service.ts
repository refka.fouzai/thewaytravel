import { Injectable } from '@angular/core';
import { Contact } from '../models/contact';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
const httpOptions= {
  headers: new HttpHeaders(
    {'Content-Type':  'application/json'}
    )
};
const urlcontact = "https://www.freedomtravel.tn/ng/addcontactform.php"; 
@Injectable({
  providedIn: 'root'
})
export class ServiceContactService {

  constructor(private http:HttpClient) { }
  addContact(contact:Contact):Observable<Contact>{
    return this.http.post<Contact>(urlcontact,contact,httpOptions);
    
  }
}
