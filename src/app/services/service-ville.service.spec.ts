import { TestBed } from '@angular/core/testing';

import { ServiceVilleService } from './service-ville.service';

describe('ServiceVilleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceVilleService = TestBed.get(ServiceVilleService);
    expect(service).toBeTruthy();
  });
});
