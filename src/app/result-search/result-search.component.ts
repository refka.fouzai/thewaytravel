import { Component, OnInit, Input } from '@angular/core';

import { Hotel } from '../models/hotel';
import { Voyage } from '../models/voyage';
import { CommonServiceService } from '../services/common-service.service';

@Component({
  selector: 'app-result-search',
  templateUrl: './result-search.component.html',
  styleUrls: ['./result-search.component.css']
})
export class ResultSearchComponent implements OnInit {
  @Input() hotels: Hotel[];
  @Input() voyages: Voyage[];
  @Input() currentRoute:string;
  imgurl:string;
  
  constructor(private commonService:CommonServiceService) { 
  
  }
  ngOnInit() {
    
    this.commonService.cast.subscribe(hotels=>{
      this.hotels = hotels;
     // console.log('nouvelle liste hotels'+this.hotels);
      });
    }
  
}
